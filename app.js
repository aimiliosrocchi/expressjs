//-----------------MODULES---------------------
//import express
const express= require('express');
const app= express();
//get path of config.env fileS

const dotenv = require("dotenv");
dotenv.config({path: 'config.env'});

//import path module
const path = require("path");

//import morgan
const morgan = require('morgan');
app.use(morgan('tiny'));

// conectivity with mongoDB Atlas Clusters
const connectDB = require('./database/db_connection');


connectDB()

//parse request to body parser
const bodyparser = require("body-parser");
app.use(bodyparser.urlencoded({extended: true}));

//set view engine 
app.set("view engine", "ejs");

//load assets globaly when '/css' triggered
app.use('/css', express.static(path.resolve(__dirname, "assets/css")));
app.use('/js', express.static(path.resolve(__dirname, "assets/js")));
app.use('/img', express.static(path.resolve(__dirname, "assets/img")));




//import user model
require('./models/user.model');

//import pois model
require('./models/pois.model');

//import routes
const pointOfInterestRouter= require('./routes/pointOfInterest')
const userRouter= require('./routes/userRoutes');//---------------------------------------------


//---------------------MIDDLEWARES----------------

//my middleware
app.use((req,res,next)=>{
    console.log("hi my friend, I am your middleware!")
    next();
});

//here we specify the route(URL) for which we wish to use our middleware(userRouter)
app.use('/api/points',pointOfInterestRouter);
app.use('/api/users',userRouter);

//render to HomePage view
app.use('/',  (req,res)=>{
    res.render('init');
})




//export app to use it in the server.js file
module.exports = app;