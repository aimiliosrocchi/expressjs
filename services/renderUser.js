const mongoose = require('mongoose');
const User = mongoose.model('users');
const axios = require('axios');


exports.homeRoutes = (req, res) =>{
  //get request to /api/users
  axios.get('http://localhost:3000/api/users/getUsers')
    .then(function(response){
      res.render('index', {users: response.data});
    })
    .catch(err=>{
      res.send(err);
    })
}

//get the view to add a new User
exports.addUser = (req,res)=>{
  res.render('add_user');
}

//get the id from url and using axios create a get request to trigger routes and controller
//also get the view for end users to update
exports.updateUser = (req, res) => {
  axios.get('http://localhost:3000/api/users/getUsers',{params :{email : req.query.email}})
    .then(function(userdata){
      res.render('update_user', {user: userdata.data})
    })
    .catch(err=>{
      res.send(err);
    })
   
}
