const mongoose = require('mongoose');
const User = mongoose.model('points_of_interests');
const axios = require('axios');

//service to upload image for points of interests
var multer = require('multer');
  
exports.homeRoutes = (req, res) => {
  //get request to /api/points
  axios.get('http://localhost:3000/api/points/getPoints')
    .then(function(response){
      res.render('points_index', {points: response.data});
    })
    .catch(err=>{
      res.send(err);
    })
}

//get the view to add a new point
exports.addPoint = (req,res)=> {
  res.render('add_point');
}

//get the name from url and using axios create a get request to trigger routes and controller
//also get the view  to update
exports.updatePoint = (req, res) => {
  axios.get('http://localhost:3000/api/points/getPoints',{params :{name : req.query.name}})
    .then(function(pointdata){
      res.render('update_point', {point: pointdata.data})
    })
    .catch(err=>{
      res.send(err);
    })
   
}

