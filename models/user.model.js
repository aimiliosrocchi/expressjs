const mongoose = require('mongoose');

const Schema = mongoose.Schema;
var usersSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
    unique: true,
  },  
  lastName: {
    type: String
    },
    age: {
      type: String
    },
});


const Users =mongoose.model('users', usersSchema);

module.exports = Users;