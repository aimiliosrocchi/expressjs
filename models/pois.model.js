const mongoose = require('mongoose');
//pois schema 
const Schema = mongoose.Schema;
var poisSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String
  },
  image: {
    type: String,
    default: 'default.jpg'
  } 
});



mongoose.model('points_of_interests', poisSchema);
