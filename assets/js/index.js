//use id on submit to alert end user
$("#add_user").submit(function(event){
  alert("Data Inserted Successfully!");
})

$("#add_point").submit(function(event){
  alert("Data Inserted Successfully!");
})

//use id on submit to create with ajax the put request to trigger updateUserById user router and user controller
$("#update_user").submit(function(event){
  event.preventDefault();

  var unindexed_array = $(this).serializeArray();
  var data = {}

  $.map(unindexed_array, function(n, i){
    data[n['name']] = n['value'];
  })
    var request ={
      "url": `http://localhost:3000/api/users/${data.email}`, 
      "method": "PATCH",
      "data": data
    } 


    $.ajax(request).done(function(response){
      alert("Data Updated Successfully!");
    })
})
//use name on submit to create with ajax the PATCH request to trigger updatePointbyName point router and point controller
$("#update_point").submit(function(event){
  event.preventDefault();

  var unindexed_array = $(this).serializeArray();
  var data = {}


  $.map(unindexed_array, function(n, i){
    data[n['name']] = n['value'];
  })

  // get the new uploaded image value and parse it into our data
  if(document.getElementById("image").files.name !== "undefined"){
      var imageValue = document.getElementById("image").files.name; 
    data["image"] = imageValue;
  }else{
    data["image"] = 'default.jsp';
  }
    var request ={
      "url": `http://localhost:3000/api/points/${data.name}`, 
      "method": "PATCH",
      "data": data
    } 


    $.ajax(request).done(function(response){
      alert("Data Updated Successfully!");
    })
})

// on delete button click tringger new ajax call to delete user by email
if(window.location.pathname=="/api/users"){
  $ondelete = $(".table tbody td a.delete");
  $ondelete.click(function(){
    var email = $(this).attr("data-email")

    var request ={
      "url": `http://localhost:3000/api/users/${email}`, 
      "method": "DELETE"
    } 
    if(confirm("Do you really want to delete this record?")){
      $.ajax(request).done(function(response){
        alert("Data Deleted Successfully!");
        location.reload()
      })
    }
  })
}
// on delete button click tringger new ajax call to delete point by name
if(window.location.pathname=="/api/points"){
  $ondelete = $(".table tbody td a.delete");
  $ondelete.click(function(){
    var name = $(this).attr("data-name")

    var request ={
      "url": `http://localhost:3000/api/points/${name}`, 
      "method": "DELETE"
    } 
    if(confirm("Do you really want to delete this record?")){
      $.ajax(request).done(function(response){
        alert("Data Deleted Successfully!");
        location.reload()
      })
    }
  })
}
