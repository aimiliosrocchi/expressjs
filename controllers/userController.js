const e = require('express');
var User = require('../models/user.model');

//create and save new user
exports.createUser = async (req,res) =>{
    //validate request
    if(!req.body){
        res.status(400).send({message: "Content can not be empty"});
        return;
    }
    //new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        lastName: req.body.lastName,
        age: req.body.age
    })
    //save user in the database
    user
        .save(user)
        .then(data=>{
            res.redirect('/api/users')
        })
        .catch(err=>{
            res.status(500).send({
                message: err.message || "Some error occured while creating a new User"
            });
        });
}
//retrieve and return all users / single user
exports.findUsers = async (req,res) =>{
    //if request query has email we need to find single user
    if(req.query.email){
        const email = req.query.email;
        User.findOne({email : email})
        .then(data=>{
            if(!data){
                res.status(404).send({message: "Not found User with email" + email})
            }else{
                res.send(data)
            }
        })
        .catch(err=>{
            res.status(500).send({message: "Error retrieving user with email" + email})
        })
        //if query does not have email we need all the users from db
    }else{
        User.find()
        .then(user =>{
            res.send(user)
        })
        .catch(err =>{
            res.status(500).send({
                message: err.message || "Some error occured while getting All Users from Database"
            });
        }) 
    }
  
}

//Update a new identified user by user email
exports.updateUserByEmail = async (req,res) => {
    if(!req.body){
    return res
    .status(400)
    .send({message: "Data to update User can not be empty"})
    }
    const email = req.params.email;
    User.findOneAndUpdate(email, req.body, {
        new: true,
    })
    .then(data=>{
        if(!data){
            res.status(404).send({message:`Cannot Update User with email ${email}. Maybe user not found!`})
        }else{
            res.send(data)
        }
    })
    .catch(err=>{
        res.status(500).send({message: "Error Update User Information"})
    })
}


//delete a user with specified user email in the request
exports.deleteUserByEmail = async (req, res) => {
    const email = req.params.email;
    User.findOneAndDelete(email)
    .then(data=>{
        if(!data){
            res.status(500).send({message: `Cannot Delete with email ${email}. Maybe email is wrong`})
        }else{
            res.send({
                message: "User was deleted succesfully!"
            })
        }
    })
    .catch(err=>{
        res.status(500).send({message: "Could not delete User with email="+ email});
    });
}




