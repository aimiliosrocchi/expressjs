//we need fs here because we open the json file
const mongoose = require('mongoose');
const POINT = mongoose.model('points_of_interests');

//post method to save new Point
    exports.newPoint = async (req, res, next) =>{
        try{
            var mybody = req.body;
            
            const inserts = mybody;
            if(req.file){
                const image = req.file.filename;
                inserts.image = image;
            }else{
                inserts.image = "default.jpg";
            }
                const point = new POINT();
                point.name = inserts.name;
                point.description = inserts.description;
                point.image = inserts.image;
                point
                .save(point)
                    .then(data=>{
                        res.redirect('/api/points')
                    })
                    .catch(err=>{
                        res.status(500).send({
                            message: err.message || "Some error occured while creating a new Point"
                        });
                    });
            
        } catch (err){
            res.status(404).json({
                status: 'fail',
                message: err.message
            });
        }
    }




//one method to find one or all points from database 
  exports.findPoints = async (req,res) => {
    //if request query has name we need to find single point
    if(req.query.name){
        const name = req.query.name;
        POINT.findOne({name : name})
        .then(data=>{
            if(!data){
                res.status(404).send({message: "Not found Point with name" + name})
            }else{
                res.send(data)
            }
        })
        .catch(err=>{
            res.status(500).send({message: "Error retrieving point with name" + name})
        })
        //if query does not have name we need all the points from db
    }else{
      POINT.find()
        .then(point =>{
            res.send(point)
        })
        .catch(err =>{
            res.status(500).send({
                message: err.message || "Some error occured while getting All Points from Database"
            });
        }) 
    }
  
    }

//patch method to update only the changed values, file request working but does not save the img to specified folder under assets/img
    exports.updatePointByName = async (req,res, next) => {
        try{
    
            if(!req.body){
            return res
            .status(400)
            .send({message: "Data to update Point can not be empty"})
            }
    const name = req.params.name;
    var mybody = req.body;
    const updates = mybody;
    if(req.file){
        const image = req.file.filename;
        updates.image = image;
    }else{
        updates.image = "default.jpg"
    }
    POINT.findOneAndUpdate(name, updates, {
        new: true,
    })
    .then(data=>{
        if(!data){
            res.status(404).send({message:`Cannot Update Point with name ${name}. Maybe Point not found!`})
        }else{
            res.send(data)
        }
    })
    .catch(err=>{
        res.status(500).send({message: "Error Update POint Information"})
    })

    } catch (err){
        res.status(404).json({
        status: 'fail',
        message: err.message
         });
    }
}


//delete a point with specified point name in the request
    exports.deletePointByName = async (req, res) => {
    const name = req.params.name;
    POINT.findOneAndDelete(name)
    .then(data=>{
        if(!data){
            res.status(500).send({message: `Cannot Delete with name ${name}. Maybe name is wrong`})
        }else{
            res.send({
                message: "Point was deleted succesfully!"
            })
        }
    })
    .catch(err=>{
        res.status(500).send({message: "Could not delete Point with name="+ name});
    });
    }