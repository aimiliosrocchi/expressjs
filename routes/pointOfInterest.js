//import express
const express= require('express');

//import Pois Controller module
const pointOfInterestController = require(`${__dirname}/../controllers/pointOfInterest`);

//lets create a new router-> it is actually a middleware
const routes = express.Router();

//import service for rendering requests to front
const services = require(`${__dirname}/../services/renderPOIs`);

//import service for upload image to POIs model
const fileUpload = require(`${__dirname}/../services/fileUpload`);

routes.route('/')
.get(services.homeRoutes)


/**
 * @description  Route for points to render requests to ejs templates
 * @method GET + POST/
 */
 routes.route('/add-point')
 .get(services.addPoint)

 /**
 * @description  Route for points to render requests to ejs templates
 * @method POST/
 */
routes.route('/addPoint')
.post(fileUpload, pointOfInterestController.newPoint)


 /**
 * @description  Route for points to render requests to ejs templates
 * @method GET/
 */
routes.route('/getPoints')
.get(pointOfInterestController.findPoints)


routes.route('/updatePoint')
.get(services.updatePoint)

routes.route('/:name')
.patch(fileUpload, pointOfInterestController.updatePointByName)
.delete(pointOfInterestController.deletePointByName)


//lets export our module!
module.exports= routes;
