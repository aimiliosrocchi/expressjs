
//import express
const express= require('express');

//import user Controller module
const userController=require(`${__dirname}/../controllers/userController`);

//import router module
const routes = express.Router();



/**
 * @description Root Route for users
 * @method GET /
 */
const services = require('../services/renderUser');
routes.route('/')
.get(services.homeRoutes)



/**
 * @description  Route for users to render requests to ejs templates
 * @method GET + POST/
 */
routes.route('/add-user')
.get(services.addUser)

/**
 * @description add user Route for users
 * @method POST/
 */
routes.route('/addUser')
.post(userController.createUser)

/**
 * @description retrieve All Users or Single User
 * @method GET /
 */
routes.route('/getUsers')
.get(userController.findUsers)

/**
 * @description update user Route for users by Id
 * @method GET + POST/
 */
routes.route('/updateUser')
.get(services.updateUser)


routes.route('/:email')
.patch(userController.updateUserByEmail)
.delete(userController.deleteUserByEmail)


//lets export our module!
module.exports= routes;