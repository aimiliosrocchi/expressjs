//call middleware
const app = require('./app');

//get the port for http server from .env
const PORT = process.env.port || 8080;

//START SERVER
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});