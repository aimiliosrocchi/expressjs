const mongoose = require('mongoose');

//replace username and password with  .env variables
var db = process.env.DATABASE_URL.replace('<username>', process.env.DB_USERNAME);
 db = db.replace('<password>', process.env.DB_PASSWORD);

 const connectDB = async () =>{
     try{
     const con = await mongoose.connect(
         db, 
         {
             useUnifiedTopology: true
          })
          console.log(`Mongo DB connected: ${con.connection.host}`);
    }catch(err){
        console.log(err);
        process.exit(1);
    }   
 }

module.exports = connectDB;